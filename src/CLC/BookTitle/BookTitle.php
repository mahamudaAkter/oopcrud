<?php
namespace App\BookTitle;
use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
use PDO;
use PDOException;
class BookTitle extends DB{
    private $id, $bookName, $AuthorName;

    public function  setData($postData){
        // var_dump($postData);
        if (array_key_exists('id', $postData)) { $this->id = $postData['id']; }
        if (array_key_exists('bookName', $postData)) { $this->bookName = $postData['bookName']; }
        if (array_key_exists('AuthorName', $postData)) { $this->AuthorName = $postData['AuthorName']; }
    }
    public function store(){
        $arrData = array($this->bookName,$this->AuthorName);
        //var_dump($arrData);
        $sql="INSERT into book (bookName,AuthorName) VALUES (?,?)";
        //var_dump($this->DBH); die();
        $STH=$this->DBH->prepare($sql);
        $result=$STH->execute($arrData);
        if($result)
            Message:: message("success");
        else
            Message::message("failled");
        Utility::redirect("index.php");
    }

    public function index(){
        $sql = "select * from book where soft_delete='NO'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
}

    public function view(){
        $sql="select * from book where id=".$this->id;
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }


    public function update(){
        $arrData = array($this->bookName,$this->AuthorName);
        //var_dump($arrData);
        $sql="UPDATE book SET bookName=?,AuthorName=? WHERE id=".$this->id;
        //var_dump($this->DBH); die();
        $STH=$this->DBH->prepare($sql);
        $result=$STH->execute($arrData);
        if($result)
            Message:: message("success");
        else
            Message::message("failled");
        Utility::redirect("index.php");
    }

    public function delete(){
        $arrData = array($this->bookName,$this->AuthorName);
        //var_dump($arrData);
        $sql="DELETE from book WHERE id=".$this->id;
        //var_dump($this->DBH); die();
        $result=$this->DBH->exec($sql);
        if($result)
            Message:: message("success");
        else
            Message::message("failled");
        Utility::redirect("index.php");
    }
}
?>