<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitaa84bcfc4e8927f449a22d3ebf875152
{
    public static $prefixLengthsPsr4 = array (
        'A' => 
        array (
            'App\\' => 4,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'App\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src/CLC',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitaa84bcfc4e8927f449a22d3ebf875152::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitaa84bcfc4e8927f449a22d3ebf875152::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
