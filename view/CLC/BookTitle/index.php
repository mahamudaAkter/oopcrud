<?php
require_once ("../../../vendor/autoload.php");
$objBookTitle =new \App\BookTitle\BookTitle();
$alldata = $objBookTitle->index();
//var_dump($alldata);

//echo count($alldata);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Book</title>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <script src="stylesheet" href="../../../resource/bootstrap/js/bootstrap.min.js"></script>

</head>
<body>
<div class="container">
    <form id="" action="" method="post">
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-10">

                <table align="center" border='solid 1px' width='50%'>
                    <tr > <th>ID</th><th>Book Name</th><th>Author Name</th> <th>Action</th> </tr>
                    <?php

                    foreach($alldata as $onedata) {
                        echo "<tr><td> $onedata->id </td><td> $onedata->bookName</td> <td> $onedata->AuthorName</td>
<td> <a href='edit.php?id=$onedata->id' class='btn btn-primary'  >Edit</a>
<a  href='delete.php?id=$onedata->id' class='btn btn-danger' >Delete</a></td> </tr>";
                    }

                    ?>

                </table>

            </div>
            <div class="col-md-1"></div>
        </div>
    </form>
</div>

</body>
</html>